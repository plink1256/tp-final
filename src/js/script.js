let mySwiper;


//Code pour transformer les codes de langue dans le nom complet
let isoLangs = {
    "ab":{
        "name":"Abkhaze",
        "nativeName":"аҧсуа"
    },
    "aa":{
        "name":"Afar",
        "nativeName":"Afaraf"
    },
    "af":{
        "name":"Afrikaans",
        "nativeName":"Afrikaans"
    },
    "ak":{
        "name":"Akan",
        "nativeName":"Akan"
    },
    "sq":{
        "name":"Albanais",
        "nativeName":"Shqip"
    },
    "am":{
        "name":"Amharique",
        "nativeName":"አማርኛ"
    },
    "ar":{
        "name":"Arabe",
        "nativeName":"العربية"
    },
    "an":{
        "name":"Aragonais",
        "nativeName":"Aragonés"
    },
    "hy":{
        "name":"Arménien",
        "nativeName":"Հայերեն"
    },
    "as":{
        "name":"Assamais",
        "nativeName":"অসমীয়া"
    },
    "av":{
        "name":"Avar",
        "nativeName":"авар мацӀ, магӀарул мацӀ"
    },
    "ae":{
        "name":"Avestique",
        "nativeName":"avesta"
    },
    "ay":{
        "name":"Aymara",
        "nativeName":"aymar aru"
    },
    "az":{
        "name":"Azéri",
        "nativeName":"azərbaycan dili"
    },
    "bm":{
        "name":"Bambara",
        "nativeName":"bamanankan"
    },
    "ba":{
        "name":"Bachkir",
        "nativeName":"башҡорт теле"
    },
    "eu":{
        "name":"Basque",
        "nativeName":"euskara, euskera"
    },
    "be":{
        "name":"Biélorusse",
        "nativeName":"Беларуская"
    },
    "bn":{
        "name":"Bengali",
        "nativeName":"বাংলা"
    },
    "bh":{
        "name":"Bihari",
        "nativeName":"भोजपुरी"
    },
    "bi":{
        "name":"Bichlamar",
        "nativeName":"Bislama"
    },
    "bs":{
        "name":"Bosniaque",
        "nativeName":"bosanski jezik"
    },
    "br":{
        "name":"Breton",
        "nativeName":"brezhoneg"
    },
    "bg":{
        "name":"Bulgare",
        "nativeName":"български език"
    },
    "my":{
        "name":"Birman",
        "nativeName":"ဗမာစာ"
    },
    "ca":{
        "name":"Catalan; Valencien",
        "nativeName":"Català"
    },
    "ch":{
        "name":"Chamorro",
        "nativeName":"Chamoru"
    },
    "ce":{
        "name":"Tchétchène",
        "nativeName":"нохчийн мотт"
    },
    "ny":{
        "name":"Chichewa; Chewa; Nyanja",
        "nativeName":"chiCheŵa, chinyanja"
    },
    "zh":{
        "name":"Chinois",
        "nativeName":"中文 (Zhōngwén), 汉语, 漢語"
    },
    "cv":{
        "name":"Tchouvaque",
        "nativeName":"чӑваш чӗлхи"
    },
    "kw":{
        "name":"Cornique",
        "nativeName":"Kernewek"
    },
    "co":{
        "name":"Corse",
        "nativeName":"corsu, lingua corsa"
    },
    "cr":{
        "name":"Cree",
        "nativeName":"ᓀᐦᐃᔭᐍᐏᐣ"
    },
    "hr":{
        "name":"Croate",
        "nativeName":"hrvatski"
    },
    "cs":{
        "name":"Tchèque",
        "nativeName":"česky, čeština"
    },
    "da":{
        "name":"Danois",
        "nativeName":"dansk"
    },
    "dv":{
        "name":"Maldivien",
        "nativeName":"ދިވެހި"
    },
    "nl":{
        "name":"Néerlandais, Flamand",
        "nativeName":"Nederlands, Vlaams"
    },
    "en":{
        "name":"Anglais",
        "nativeName":"English"
    },
    "eo":{
        "name":"Espéranto",
        "nativeName":"Esperanto"
    },
    "et":{
        "name":"Estonien",
        "nativeName":"eesti, eesti keel"
    },
    "ee":{
        "name":"Éwé",
        "nativeName":"Eʋegbe"
    },
    "fo":{
        "name":"Féroïen",
        "nativeName":"føroyskt"
    },
    "fj":{
        "name":"Fijien",
        "nativeName":"vosa Vakaviti"
    },
    "fi":{
        "name":"Finnois",
        "nativeName":"suomi, suomen kieli"
    },
    "fr":{
        "name":"Français",
        "nativeName":"français, langue française"
    },
    "ff":{
        "name":"Peul",
        "nativeName":"Fulfulde, Pulaar, Pular"
    },
    "gl":{
        "name":"Galicien",
        "nativeName":"Galego"
    },
    "ka":{
        "name":"Géorgien",
        "nativeName":"ქართული"
    },
    "de":{
        "name":"Allemand",
        "nativeName":"Deutsch"
    },
    "el":{
        "name":"Grec moderne",
        "nativeName":"Ελληνικά"
    },
    "gn":{
        "name":"Guarani",
        "nativeName":"Avañeẽ"
    },
    "gu":{
        "name":"Goudjrati",
        "nativeName":"ગુજરાતી"
    },
    "ht":{
        "name":"Haïtien, Créole haïtien",
        "nativeName":"Kreyòl ayisyen"
    },
    "ha":{
        "name":"Haoussa",
        "nativeName":"Hausa, هَوُسَ"
    },
    "he":{
        "name":"Hébreu",
        "nativeName":"עברית"
    },
    "hz":{
        "name":"Herero",
        "nativeName":"Otjiherero"
    },
    "hi":{
        "name":"Hindi",
        "nativeName":"हिन्दी, हिंदी"
    },
    "ho":{
        "name":"Hiri Motu",
        "nativeName":"Hiri Motu"
    },
    "hu":{
        "name":"Hongrois",
        "nativeName":"Magyar"
    },
    "ia":{
        "name":"Interlingua (langue auxiliaire internationale)",
        "nativeName":"Interlingua"
    },
    "id":{
        "name":"Indonésien",
        "nativeName":"Bahasa Indonesia"
    },
    "ie":{
        "name":"Interlingue",
        "nativeName":"Originally called Occidental; then Interlingue after WWII"
    },
    "ga":{
        "name":"Irlandais",
        "nativeName":"Gaeilge"
    },
    "ig":{
        "name":"Igbo",
        "nativeName":"Asụsụ Igbo"
    },
    "ik":{
        "name":"Inupiaq",
        "nativeName":"Iñupiaq, Iñupiatun"
    },
    "io":{
        "name":"Ido",
        "nativeName":"Ido"
    },
    "is":{
        "name":"Islandais",
        "nativeName":"Íslenska"
    },
    "it":{
        "name":"Italien",
        "nativeName":"Italiano"
    },
    "iu":{
        "name":"Inuktitut",
        "nativeName":"ᐃᓄᒃᑎᑐᑦ"
    },
    "ja":{
        "name":"Japonais",
        "nativeName":"日本語 (にほんご／にっぽんご)"
    },
    "jv":{
        "name":"Javanais",
        "nativeName":"basa Jawa"
    },
    "kl":{
        "name":"Groenlandais",
        "nativeName":"kalaallisut, kalaallit oqaasii"
    },
    "kn":{
        "name":"Kannada",
        "nativeName":"ಕನ್ನಡ"
    },
    "kr":{
        "name":"Kanouri",
        "nativeName":"Kanuri"
    },
    "ks":{
        "name":"Kashmiri",
        "nativeName":"कश्मीरी, كشميري‎"
    },
    "kk":{
        "name":"Kazakh",
        "nativeName":"Қазақ тілі"
    },
    "km":{
        "name":"Khmer",
        "nativeName":"ភាសាខ្មែរ"
    },
    "ki":{
        "name":"Kikuyu, Gikuyu",
        "nativeName":"Gĩkũyũ"
    },
    "rw":{
        "name":"Kinyarwanda",
        "nativeName":"Ikinyarwanda"
    },
    "ky":{
        "name":"Kirghiz, Kyrgyz",
        "nativeName":"кыргыз тили"
    },
    "kv":{
        "name":"Kom",
        "nativeName":"коми кыв"
    },
    "kg":{
        "name":"Kongo",
        "nativeName":"KiKongo"
    },
    "ko":{
        "name":"Coréen",
        "nativeName":"한국어 (韓國語), 조선말 (朝鮮語)"
    },
    "ku":{
        "name":"Kurde",
        "nativeName":"Kurdî, كوردی‎"
    },
    "kj":{
        "name":"Kwanyama, Kuanyama",
        "nativeName":"Kuanyama"
    },
    "la":{
        "name":"Latin",
        "nativeName":"latine, lingua latina"
    },
    "lb":{
        "name":"Luxenbourgeois",
        "nativeName":"Lëtzebuergesch"
    },
    "lg":{
        "name":"Ganda",
        "nativeName":"Luganda"
    },
    "li":{
        "name":"Limbourgeois",
        "nativeName":"Limburgs"
    },
    "ln":{
        "name":"Lingala",
        "nativeName":"Lingála"
    },
    "lo":{
        "name":"Lao",
        "nativeName":"ພາສາລາວ"
    },
    "lt":{
        "name":"Lithuanien",
        "nativeName":"lietuvių kalba"
    },
    "lu":{
        "name":"Luba-Katanga",
        "nativeName":""
    },
    "lv":{
        "name":"Letton",
        "nativeName":"latviešu valoda"
    },
    "gv":{
        "name":"Manx",
        "nativeName":"Gaelg, Gailck"
    },
    "mk":{
        "name":"Macédonien",
        "nativeName":"македонски јазик"
    },
    "mg":{
        "name":"Malagache",
        "nativeName":"Malagasy fiteny"
    },
    "ms":{
        "name":"Malais",
        "nativeName":"bahasa Melayu, بهاس ملايو‎"
    },
    "ml":{
        "name":"Malayalam",
        "nativeName":"മലയാളം"
    },
    "mt":{
        "name":"Maltais",
        "nativeName":"Malti"
    },
    "mi":{
        "name":"Māori",
        "nativeName":"te reo Māori"
    },
    "mr":{
        "name":"Marathe",
        "nativeName":"मराठी"
    },
    "mh":{
        "name":"Marshall",
        "nativeName":"Kajin M̧ajeļ"
    },
    "mn":{
        "name":"Mongol",
        "nativeName":"монгол"
    },
    "na":{
        "name":"Nauruan",
        "nativeName":"Ekakairũ Naoero"
    },
    "nv":{
        "name":"Navaho",
        "nativeName":"Diné bizaad, Dinékʼehǰí"
    },
    "nb":{
        "name":"Bokmål",
        "nativeName":"Norsk bokmål"
    },
    "nd":{
        "name":"Ndébélé du Nord",
        "nativeName":"isiNdebele"
    },
    "ne":{
        "name":"Népalais",
        "nativeName":"नेपाली"
    },
    "ng":{
        "name":"Ndonga",
        "nativeName":"Owambo"
    },
    "nn":{
        "name":"Nynorsk",
        "nativeName":"Norsk nynorsk"
    },
    "no":{
        "name":"Norvégien",
        "nativeName":"Norsk"
    },
    "ii":{
        "name":"Yi de Sichuan",
        "nativeName":"ꆈꌠ꒿ Nuosuhxop"
    },
    "nr":{
        "name":"Ndébélé du Sud",
        "nativeName":"isiNdebele"
    },
    "oc":{
        "name":"Occitan",
        "nativeName":"Occitan"
    },
    "oj":{
        "name":"Ojibwa",
        "nativeName":"ᐊᓂᔑᓈᐯᒧᐎᓐ"
    },
    "cu":{
        "name":"Vieux Slave",
        "nativeName":"ѩзыкъ словѣньскъ"
    },
    "om":{
        "name":"Galla",
        "nativeName":"Afaan Oromoo"
    },
    "or":{
        "name":"Oriya",
        "nativeName":"ଓଡ଼ିଆ"
    },
    "os":{
        "name":"Ossetian, Ossetic",
        "nativeName":"ирон æвзаг"
    },
    "pa":{
        "name":"Pendjabi",
        "nativeName":"ਪੰਜਾਬੀ, پنجابی‎"
    },
    "pi":{
        "name":"Pāli",
        "nativeName":"पाऴि"
    },
    "fa":{
        "name":"Persan",
        "nativeName":"فارسی"
    },
    "pl":{
        "name":"Polonais",
        "nativeName":"polski"
    },
    "ps":{
        "name":"Pachto",
        "nativeName":"پښتو"
    },
    "pt":{
        "name":"Portugais",
        "nativeName":"Português"
    },
    "qu":{
        "name":"Quechua",
        "nativeName":"Runa Simi, Kichwa"
    },
    "rm":{
        "name":"Romanche",
        "nativeName":"rumantsch grischun"
    },
    "rn":{
        "name":"Rundi",
        "nativeName":"kiRundi"
    },
    "ro":{
        "name":"Roumain, Moldave",
        "nativeName":"română"
    },
    "ru":{
        "name":"Russe",
        "nativeName":"русский язык"
    },
    "sa":{
        "name":"Sanskrit (Saṁskṛta)",
        "nativeName":"संस्कृतम्"
    },
    "sc":{
        "name":"Sarde",
        "nativeName":"sardu"
    },
    "sd":{
        "name":"Sindhi",
        "nativeName":"सिन्धी, سنڌي، سندھی‎"
    },
    "se":{
        "name":"Sami du Nord",
        "nativeName":"Davvisámegiella"
    },
    "sm":{
        "name":"Samoan",
        "nativeName":"gagana faa Samoa"
    },
    "sg":{
        "name":"Sango",
        "nativeName":"yângâ tî sängö"
    },
    "sr":{
        "name":"Serbe",
        "nativeName":"српски језик"
    },
    "gd":{
        "name":"Gaélique",
        "nativeName":"Gàidhlig"
    },
    "sn":{
        "name":"Shona",
        "nativeName":"chiShona"
    },
    "si":{
        "name":"Singhalais",
        "nativeName":"සිංහල"
    },
    "sk":{
        "name":"Slovaque",
        "nativeName":"slovenčina"
    },
    "sl":{
        "name":"Slovène",
        "nativeName":"slovenščina"
    },
    "so":{
        "name":"Somali",
        "nativeName":"Soomaaliga, af Soomaali"
    },
    "st":{
        "name":"Sotho du Sud",
        "nativeName":"Sesotho"
    },
    "es":{
        "name":"Espagnol",
        "nativeName":"español, castellano"
    },
    "su":{
        "name":"Soundanais",
        "nativeName":"Basa Sunda"
    },
    "sw":{
        "name":"Swahili",
        "nativeName":"Kiswahili"
    },
    "ss":{
        "name":"Swati",
        "nativeName":"SiSwati"
    },
    "sv":{
        "name":"Suédois",
        "nativeName":"svenska"
    },
    "ta":{
        "name":"Tamoul",
        "nativeName":"தமிழ்"
    },
    "te":{
        "name":"Télougou",
        "nativeName":"తెలుగు"
    },
    "tg":{
        "name":"Tadjik",
        "nativeName":"тоҷикӣ, toğikī, تاجیکی‎"
    },
    "th":{
        "name":"Thaï",
        "nativeName":"ไทย"
    },
    "ti":{
        "name":"Tigrigna",
        "nativeName":"ትግርኛ"
    },
    "bo":{
        "name":"Tibétain",
        "nativeName":"བོད་ཡིག"
    },
    "tk":{
        "name":"Turkmène",
        "nativeName":"Türkmen, Түркмен"
    },
    "tl":{
        "name":"Tagalog",
        "nativeName":"Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔"
    },
    "tn":{
        "name":"Tswana",
        "nativeName":"Setswana"
    },
    "to":{
        "name":"Tongan",
        "nativeName":"faka Tonga"
    },
    "tr":{
        "name":"Turc",
        "nativeName":"Türkçe"
    },
    "ts":{
        "name":"Tsonga",
        "nativeName":"Xitsonga"
    },
    "tt":{
        "name":"Tatar",
        "nativeName":"татарча, tatarça, تاتارچا‎"
    },
    "tw":{
        "name":"Twi",
        "nativeName":"Twi"
    },
    "ty":{
        "name":"Tahitien",
        "nativeName":"Reo Tahiti"
    },
    "ug":{
        "name":"Ouïgour",
        "nativeName":"Uyƣurqə, ئۇيغۇرچە‎"
    },
    "uk":{
        "name":"Ukrainien",
        "nativeName":"українська"
    },
    "ur":{
        "name":"Ourdou",
        "nativeName":"اردو"
    },
    "uz":{
        "name":"Ouszbek",
        "nativeName":"zbek, Ўзбек, أۇزبېك‎"
    },
    "ve":{
        "name":"Venda",
        "nativeName":"Tshivenḓa"
    },
    "vi":{
        "name":"Vietnamien",
        "nativeName":"Tiếng Việt"
    },
    "vo":{
        "name":"Volapük",
        "nativeName":"Volapük"
    },
    "wa":{
        "name":"Walloon",
        "nativeName":"Walon"
    },
    "cy":{
        "name":"Galois",
        "nativeName":"Cymraeg"
    },
    "wo":{
        "name":"Wolof",
        "nativeName":"Wollof"
    },
    "fy":{
        "name":"Frison occidental",
        "nativeName":"Frysk"
    },
    "xh":{
        "name":"Xhosa",
        "nativeName":"isiXhosa"
    },
    "yi":{
        "name":"Yiddish",
        "nativeName":"ייִדיש"
    },
    "yo":{
        "name":"Yoruba",
        "nativeName":"Yorùbá"
    },
    "za":{
        "name":"Zhuang, Chuang",
        "nativeName":"Saɯ cueŋƅ, Saw cuengh"
    }
};
let getLanguageName = function(key) {
    key = key.slice(0,2);
    var lang = isoLangs[key];
    return lang ? lang.name : undefined;
};
window.getLanguageName = getLanguageName;
//-------------------------------------------------------------

document.addEventListener('DOMContentLoaded', function () {

    //Code pour faire le Swiper
    if(document.querySelector('.swiper-container')){
        mySwiper = new Swiper ('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',

            slidesPerView: 5,
            centeredSlide: true,
            grabCursor: true,

            loop: true,

            autoplay: {

                delay: 3500,

            },

            breakpoints: {

                660:{
                   slidesPerView: 1,
                },

                900:{
                    slidesPerView: 2,
                },

                1200:{
                  slidesPerView: 3,
                },

                1440:{
                    slidesPerView: 4,
                }

            },


            // Navigation arrows
            navigation: {
                nextEl: '.fa-chevron-right',
                prevEl: '.fa-chevron-left',
            },

        });
    }


    let connexion = new MovieDB();


    //Code pour le bouton remonter dans le footer
    if(document.querySelector('.btn-up')){
        document.querySelector('.btn-up').addEventListener('click',function () {
            window.scroll({
                top: 0,
                behavior: "smooth"
            })
        });
    }


    //Code pour les boutons like / dislike dans la section de commentaire
    if(document.querySelectorAll('.far.fa-thumbs-down')){

        document.querySelectorAll('.far.fa-thumbs-down').forEach(function (i) {
            i.addEventListener('click', function () {
                i.classList.toggle('far');
                i.classList.toggle('fas');

                if(i.classList.contains('fas')){
                    i.style.color = '#FFF208';
                    i.nextSibling.textContent++;
                }
                else{
                    i.style.color = "inherit";
                    i.nextSibling.textContent--;
                }
            })
        });

        document.querySelectorAll('.far.fa-thumbs-up').forEach(function (i) {
            i.addEventListener('click', function () {
                i.classList.toggle('far');
                i.classList.toggle('fas');

                if(i.classList.contains('fas')){
                    i.style.color = '#FFF208';
                    i.nextSibling.textContent++;
                }
                else{
                    i.style.color = "inherit";
                    i.nextSibling.textContent--;
                }
            })
        });
    }


    //Requetes des éléments
    if(location.pathname.search("fiche-film.html") > 0){

        let parametres =(new URL(document.location)).searchParams;

        connexion.requeteInfosFilm(parametres.get('id'));
        connexion.requeteActeurFilms(parametres.get('id'));



    }
    else{

        connexion.requetePresentementEnSalle();
        connexion.requeteFilmsPopulaire();
    }


});



class MovieDB{

    constructor(){

        this.APIKey = "4fe9e2974bf05abb1f0346b717593c2b";
        this.lang = 'fr-CA';
        this.baseURL = 'https://api.themoviedb.org/3/';
        this.imgURL = 'https://image.tmdb.org/t/p/';
        this.widthPoster = ["92", "154", "185", "342", "500", "780", "original"];
        this.widthActor = ["45", "185", "original"];
        this.totalFilms = 8;
        this.totalActeurs = 6;

    }

    requeteFilmsPopulaire(){

        let xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", function (e) {

            let target = e.currentTarget;

            if(target.readyState === target.DONE){

                let data = JSON.parse(target.responseText).results;

                //Boucle pour faire les 6 films
                for(let i = 0; i < 6; i++){

                    //Créer la copie du template
                    let divFilm = document.querySelector('div.template.film-populaire').cloneNode(true);

                    //Set du lien de l'article
                    divFilm.querySelector('a').setAttribute('href', "fiche-film.html?id="+data[i].id);

                    //Set l'image (src et alt) de l'article
                    divFilm.querySelector('img').setAttribute('src', this.imgURL + "w" + this.widthPoster[3] + data[i].poster_path);
                    divFilm.querySelector('img').setAttribute('alt', data[i].title);

                    //Set le titre pour le titre du film
                    divFilm.querySelector('h1').innerText = data[i].title;

                    //SI aucune description n'Est disponible
                    if( data[i].overview === "" ){

                        
                        //Deuxième requête en Anglais pour avoir la description en Anglais
                        let xhr2 = new XMLHttpRequest();

                        xhr2.addEventListener("readystatechange", function (e) {

                            let targetEn = e.currentTarget;

                            if(targetEn.readyState === targetEn.DONE){

                                let descriptionTraduite = JSON.parse(targetEn.responseText).overview;
                                
                                divFilm.querySelector('p.description').innerText = "Description non disponible en Français. \n La description en Anglais : \n" + descriptionTraduite;
                            }

                        }.bind(this));

                        xhr2.open("GET", this.baseURL + "movie/" + data[i].id + "?language=en-US&api_key=" + this.APIKey);

                        xhr2.send();

                    }
                    //SINON met la description en Francais
                    else{
                        
                        divFilm.querySelector('p.description').innerText = data[i].overview;
                    }

                    //Si la note est > 0 (donc il y a une note)
                    if(data[i].vote_count > 0){

                        let note = data[i].vote_average/2;

                        let etoiles;

                        divFilm.querySelector('div.etoile').innerHTML = '';
                        
                        //Convertit la note en étoiles
                        for(let i = 0; i < 5; i++){

                            if(i < Math.floor(note)){
                                etoiles = document.querySelector("div.templates div.etoiles i.fa-star").cloneNode(true);
                            }
                            else if( i < note){
                                etoiles = document.querySelector("div.templates div.etoiles i.fa-star-half-alt").cloneNode(true);
                            }
                            else if( i >= note){
                                etoiles = document.querySelector("div.templates div.etoiles i.fa-star.far").cloneNode(true);
                            }
                            
                            //Ajoute les étoiles dans l'article
                            divFilm.querySelector('div.etoile').appendChild(etoiles);
                        }
                    }
                    //SINON aucune note n'est disponible
                    else{
                        divFilm.querySelector('div.etoile').innerText = "Non disponible.";
                    }

                    //Ajoute l'article dans le conteneur
                    document.querySelector('article div.liste-films').appendChild(divFilm);

                }
            }
        }.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

    }

    requetePresentementEnSalle(){

        let xhr = new XMLHttpRequest();

        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIKey + "&region=FR");

        xhr.send();

        xhr.addEventListener("readystatechange", function (e) {

            let target = e.currentTarget;

            if(target.readyState === target.DONE){
                let dataNowPlaying = JSON.parse(target.responseText).results;

                //Pour savoir la hauteur maxiamle des images dans le carrousel
                let maxHeight = 0;

                for(let i = 0; i < 10; i++){

                    //Clone la slide de base du carrousel
                    let elemCarrousel = document.querySelector('div.templates > div.swiper-slide.template').cloneNode(true);

                    //Set l'image (src et alt) de la slide du carrousel
                    elemCarrousel.querySelector('img').setAttribute('src', this.imgURL + "w" + this.widthPoster[3] + dataNowPlaying[i].poster_path);
                    elemCarrousel.querySelector('img').setAttribute('alt', dataNowPlaying[i].title);


                    //SI l'image du carrousel est plus petite que les autres
                    if( elemCarrousel.querySelector('img').height > maxHeight){

                        maxHeight = elemCarrousel.querySelector('img').height;

                    }
                    else if(elemCarrousel.querySelector('img').height < maxHeight){

                        elemCarrousel.querySelector('img').style.transform = "scale(1.2)";

                    }


                    //Set le titre de la slide pour le nom du film
                    elemCarrousel.querySelector('div.contenu h2').innerText = dataNowPlaying[i].title;

                    //Set la release_date et conversion en fr-FR
                    let date = new Date(dataNowPlaying[i].release_date);
                    elemCarrousel.querySelector('div.contenu p.date').innerText = date.toLocaleDateString('fr-FR');

                    //Set du lien
                    elemCarrousel.querySelector('a').setAttribute('href', "fiche-film.html?id="+dataNowPlaying[i].id);


                    //SI une note est dispo -> Transforme en étoile
                    if(dataNowPlaying[i].vote_count > 0){

                        let note = dataNowPlaying[i].vote_average/2;

                        let etoiles;

                        for(let i = 0; i < 5; i++){

                            if(i < Math.floor(note)){
                                etoiles = document.querySelector("div.templates div.etoiles i.fa-star").cloneNode(true);
                            }
                            else if( i < note){
                                etoiles = document.querySelector("div.templates div.etoiles i.fa-star-half-alt").cloneNode(true);
                            }
                            else if( i >= note){
                                etoiles = document.querySelector("div.templates div.etoiles i.fa-star.far").cloneNode(true);
                            }

                            elemCarrousel.querySelector('div.contenu div.etoiles').appendChild(etoiles);
                        }
                    }
                    else{
                        elemCarrousel.querySelector('div.contenu div.etoiles').innerText = "Non disponible.";
                    }

                    //Ajouter les slides au carrousel
                    mySwiper.appendSlide(elemCarrousel);

                }

            }
        }.bind(this));
    }

    requeteInfosFilm(id){

        let xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", function (e) {

            let target = e.currentTarget;

            if (target.readyState === target.DONE) {

                let dataFilm = JSON.parse(target.responseText);

                //Change le title de la page pour le nom du film
                document.querySelector('head title').innerText = "F-A : "+dataFilm.title;

                //Mets les infos du film
                document.querySelector('.infoFilm-titre').innerText = dataFilm.title;
                document.querySelector('.infoFilm-poster').setAttribute('src', this.imgURL + "w" + this.widthPoster[5] + dataFilm.poster_path);
                document.querySelector('.infoFilm-poster').setAttribute('alt', dataFilm.title);

                //Change la date pour le format local FR
                let date = new Date(dataFilm.release_date);
                document.querySelector('.infoFilm-date').innerText = date.toLocaleDateString('fr-FR');

                //Set le budget
                if(dataFilm.budget === 0){
                    document.querySelector('.infoFilm-budget').innerText = "Non disponible";
                }
                else{
                    document.querySelector('.infoFilm-budget').innerText = dataFilm.budget.toLocaleString('fr-CA', {style: 'currency', currency: 'USD'});
                }

                //Set le revenue
                if(dataFilm.revenue === 0){
                    document.querySelector('.infoFilm-recette').innerText = "Non disponible";
                }
                else{
                    document.querySelector('.infoFilm-recette').innerText = dataFilm.revenue.toLocaleString('fr-CA', {style: 'currency', currency: 'USD'});
                }

                //Set la langue d'origine -> Utilise la fonction getLanguageName pour transformer le language code en nom complet
                document.querySelector('.infoFilm-langOri').innerText = getLanguageName(dataFilm.original_language);

                //Set la longeur du film
                document.querySelector('.infoFilm-length').innerText = dataFilm.runtime + " min";

                //Set les étoiles
                if(dataFilm.vote_count > 0){

                    let note = dataFilm.vote_average/2;

                    let etoiles;

                    for(let i = 0; i < 5; i++){

                        if(i < Math.floor(note)){
                            etoiles = document.querySelector("div.templates div.etoiles i.fa-star").cloneNode(true);
                        }
                        else if( i < note){
                            etoiles = document.querySelector("div.templates div.etoiles i.fa-star-half-alt").cloneNode(true);
                        }
                        else if( i >= note){
                            etoiles = document.querySelector("div.templates div.etoiles i.fa-star.far").cloneNode(true);
                        }

                        document.querySelector('div.etoiles').appendChild(etoiles);
                    }
                }
                else{
                    document.querySelector('div.etoiles').innerText = "Non disponible.";
                }


                //SI aucune description n'Est dispo -> Chercher la description en anglais
                if( dataFilm.overview === "" ){

                    let xhr2 = new XMLHttpRequest();

                    xhr2.addEventListener("readystatechange", function (e) {

                        let targetEn = e.currentTarget;

                        if(targetEn.readyState === targetEn.DONE){

                            let descriptionTraduite = JSON.parse(targetEn.responseText).overview;

                            document.querySelector('.infoFilm-description').innerText = "Description non disponible en Français. \n La description en Anglais : \n" + descriptionTraduite;
                        }

                    }.bind(this));

                    xhr2.open("GET", this.baseURL + "movie/" + id + "?language=en-US&api_key=" + this.APIKey);

                    xhr2.send();

                }
                else{
                    document.querySelector('.infoFilm-description').innerText = dataFilm.overview;
                }

            }
        }.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + id + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

    }

    requeteActeurFilms(id){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", function (e) {

            let target = e.currentTarget;

            if (target.readyState === target.DONE) {
                let dataActeurs = JSON.parse(target.responseText).cast;

                for(let i = 0; i < 4; i++){

                    //Clone le profil acteur template
                    let infoActeur = document.querySelector('.infoActeur').cloneNode(true);

                    //Set l'image (src et alt) du profil acteur
                    infoActeur.querySelector('.infoActeur-image').setAttribute('src',this.imgURL + "w" + this.widthActor[1] + dataActeurs[i].profile_path);
                    infoActeur.querySelector('.infoActeur-image').setAttribute('alt', dataActeurs[i].name);

                    //SI error avec l'image de profil de l'acteur -> Met un filer
                    infoActeur.querySelector('.infoActeur-image').addEventListener('error', function () {
                        if(dataActeurs[i].gender === 0){
                            this.setAttribute('src', "images/acteur-vide-f.png");
                        }
                        else if(dataActeurs[i].gender === 2){
                            this.setAttribute('src', "images/acteur-vide-m.png");
                        }
                        else{
                            this.setAttribute('src', "images/acteur-vide.png");
                        }
                    });

                    //Set le nom de l'acteur
                    infoActeur.querySelector('.infoActeur-nom').innerText = dataActeurs[i].name;

                    //Set le nom du personnage
                    infoActeur.querySelector('.infoActeur-personnage').innerText = dataActeurs[i].character;

                    //Ajoute le profil dans le parent
                    document.querySelector('.infoFilm-sectionActeur .wrapper .liste-acteur').appendChild(infoActeur);

                }

            }
        }.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + id + "/credits?api_key=" + this.APIKey);

        xhr.send();

    }

}